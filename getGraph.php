<?php
    require_once ("mysqlConnect.php");
    $query = mysqli_query($connect, "SELECT `dt`, `value` FROM `geo` order by `dt`");

    $dates = array();
    $values = array();

    while ($row = mysqli_fetch_array($query)) {
        $values[] = floatval($row['value']);
        $dates[] = $row['dt'];
    }
    echo json_encode(['labels' => $dates, 'values' => $values]);


?>