<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="datatables/css/jquery.dataTables.min.css">
</head>
<body>
    <h2>График</h2>
    <div style="width: 100%; height: 500px; overflow-x: scroll">
        <div style="width: 3000px; height: 90% margin: 15px">
            <canvas id="myChart" width="3000" height="450"></canvas>
        </div>
    </div>
    <h2>Таблица разрывов</h2>
    <div style="margin: 15px">
        <table id="myTable"></table>
    </div>
    <h2>
        Пройденное расстояние: <span id="km"></span> км.
    </h2>
    <a href="https://mapgroup.com.ua/glavnaya/astronomicheskie-kalkulyatory/1009-rasstoyanie-mezhdu-dvumya-koordinatamirasstoyanie-mezhdu-dvumya-koordinatami">
        Алгоритм расчета
    </a>
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="charts/dist/Chart.js"></script>
    <script type="text/javascript" src="datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="index.js"></script>
</body>