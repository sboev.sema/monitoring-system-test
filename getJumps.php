<?php
    require_once ("mysqlConnect.php");

    $query = mysqli_query($connect, "SELECT `dt`, `value` FROM `geo` order by `dt`");
    $records = array();

    $arr = mysqli_fetch_all($query, MYSQLI_ASSOC);
    $j=-1;
    for ($i = 0; $i < $query->num_rows - 1; $i++){
        $diff = round(abs(floatval($arr[$i]['value']) - floatval($arr[$i+1]['value'])),2);
        if ($diff > 10){
            if(isset($records[$j]) && $records[$j]->dateEnd == $arr[$i]['dt']){
                $records[$j]->dateEnd = $arr[$i+1]['dt'];
                $records[$j]->diff += $diff;
            }
            else {
                $j++;
                $records[$j] = new JumpRecord($arr[$i]['dt'], $arr[$i + 1]['dt'], $diff);
            }
        }
    }
    //var_dump($records);
    echo json_encode($records);

    class JumpRecord{
        public $dateStart;
        public $dateEnd;
        public $diff;
        function __construct($dateStart, $dateEnd, $diff)
        {
            $this->dateStart = $dateStart;
            $this->dateEnd = $dateEnd;
            $this->diff = $diff;
        }
    }
?>