<?php
    require_once('mysqlConnect.php');
    const EARTH_RADIUS = 6372.795;
    $query = mysqli_query
    (
        $connect,
        "select RADIANS(n.lat) lat, RADIANS(n.lon) lon from ( select @npp:=@npp+1 npp , @ngrp:=if(@grp1=t.lat and @grp2=t.lon , @ngrp+1 , least(1, (@grp1:=t.lat),(@grp2:=t.lon))) ngrp , t.* , @npp-@ngrp grp from geo t ,(select @npp:=0,@grp1:=null,@grp2:=null,@ngrp:=0)v order by t.dt )n group by n.grp"
    );
    $arr = mysqli_fetch_all($query, MYSQLI_ASSOC);
    $sumDist = 0;
    $sumDist2 = 0;
    $sumDist3 = 0;
    for ($i = 0; $i < $query->num_rows - 1; $i++) {
        $lat1 = floatval($arr[$i]['lat']);
        $lat2 = floatval($arr[$i+1]['lat']);
        $lon1 = floatval($arr[$i]['lon']);
        $lon2 = floatval($arr[$i+1]['lon']);
        $deltaLat = abs($lat1 - $lat2);
        $deltaLon = abs($lon1 - $lon2);

        $deltaSigma = 2*asin(sqrt(pow(sin($deltaLat/2),2) +
            cos($lat1)*cos($lat2)*pow(sin($deltaLon/2),2)));
        $sumDist += EARTH_RADIUS * $deltaSigma;
    }
    echo round($sumDist, 3);
?>