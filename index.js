$(document).ready(
   function() {
       $.ajax({
           url: 'getGraph.php',
           dataType: "json",
           success: function (data) {
               var ctx = $('#myChart');
               var myChart = new Chart(ctx, {
                   type: 'line',
                   data: {
                       labels: data.labels,
                       datasets: [{
                           label: 'Values',
                           data: data.values,
                           backgroundColor: [
                               'rgba(99, 99, 99, 0.2)',
                           ],
                           borderColor: [
                               'rgba(99, 99, 99, 1)',
                           ],
                           borderWidth: 1
                       }]
                   },
                   options: {
                       responsive: false,
                       scales: {
                           yAxes: [{
                               ticks: {
                                   beginAtZero: true
                               }
                           }]
                       }
                   }
               });
           }
       });


       $.ajax({
           url: 'getJumps.php',
           dataType: "json",
           success: function (data) {
               let dataTable = $('#myTable').DataTable({
                   data: data,
                   columns: [
                       {data: 'dateStart', title: 'Начало резкого изменения'},
                       {data: 'dateEnd', title: 'Конец резкого изменения'},
                       {data: 'diff', title: 'Разница уровней'}
                   ]
               });
           }
       });

       $.ajax({
           url: 'getDistance.php',
           dataType: "json",
           success: function (data) {
               console.log(data);
               $('#km').text(data);
           }
       });
   }
);
